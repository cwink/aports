# Contributor: Alex McGrath <amk@amk.ie>
# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=py3-dataclasses-json
_pkgname=dataclasses-json
pkgver=0.5.14
pkgrel=0
pkgdesc="Provides a simple API for encoding and decoding dataclasses to and from JSON."
url="https://github.com/lidatong/dataclasses-json"
arch="all"
license="MIT"
depends="python3 py3-marshmallow py3-marshmallow-enum py3-typing_inspect py3-stringcase"
makedepends="py3-gpep517 py3-poetry-core py3-installer"
checkdepends="py3-pytest py3-hypothesis py3-mypy"
subpackages="$pkgname-pyc"
source="dataclasses-json-$pkgver.tar.gz::https://github.com/lidatong/dataclasses-json/archive/v$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

prepare() {
	sed -i "s/0\.0\.0/$pkgver/" pyproject.toml
	default_prepare
}

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
5ec21843ef29a8e6e9511922698f42c115c7e641ca9ae5e47a58c6f0697a51928fd462f6342190263469baa70b76a302ad1d27954f50b9457b97149586f35703  dataclasses-json-0.5.14.tar.gz
"
