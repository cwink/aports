# Maintainer: Cowington Post <cowingtonpost@gmail.com>
pkgname=py3-minio
pkgver=7.1.15
pkgrel=0
pkgdesc="MinIO client SDK for Python"
url="https://docs.min.io/docs/python-client-quickstart-guide.html"
arch="noarch"
license="Apache-2.0"
depends="py3-certifi py3-urllib3"
makedepends="py3-gpep517 py3-setuptools py3-wheel"
checkdepends="py3-pytest"
subpackages="$pkgname-pyc"
source="https://github.com/minio/minio-py/archive/$pkgver/py3-minio-$pkgver.tar.gz"
builddir="$srcdir/minio-py-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
30af48160ff6ab5a7378f661924a03c0953f8ee4580ac9224e5bbf1443e0d3fd6b73f0fa46ecb0538b39c1f7439f91cd15635fb07448ff695918c96d1de5adcc  py3-minio-7.1.15.tar.gz
"
