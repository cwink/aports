# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Maintainer: Andy Postnikov <apostnikov@gmail.com>
pkgname=php81-pecl-phalcon
_extname=phalcon
pkgver=5.3.0
pkgrel=0
pkgdesc="High performance, full-stack PHP 8.1 framework delivered as a C extension"
url="https://phalcon.io/"
arch="all"
license="BSD-3-Clause"
_phpv=81
_php=php$_phpv
depends="
	$_php-curl
	$_php-fileinfo
	$_php-gettext
	$_php-mbstring
	$_php-openssl
	$_php-pdo
	$_php-session
	$_php-pecl-psr
	"
makedepends="$_php-dev"
source="php-$_extname-$pkgver.tgz::https://pecl.php.net/get/$_extname-$pkgver.tgz"
builddir="$srcdir/$_extname-$pkgver"

build() {
	phpize$_phpv
	./configure --prefix=/usr --with-php-config=/usr/bin/php-config$_phpv
	make
}

check() {
	# no tests provided
	$_php -d extension=modules/$_extname.so --ri $_extname
}

package() {
	make INSTALL_ROOT="$pkgdir" install

	local _confdir="$pkgdir"/etc/$_php/conf.d
	install -d $_confdir
	echo "extension=$_extname" > $_confdir/$_extname.ini
}

sha512sums="
7eed5394d7465b409a8e3b4b5a221f58af35bac68f0d8d2ae89ee3ea28a3becef2d2b27b40225c38e6c508ff83974a0bdfa4e588c77187586bb7990024c101c7  php-phalcon-5.3.0.tgz
"
