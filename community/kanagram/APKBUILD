# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kanagram
pkgver=23.04.3
pkgrel=1
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://edu.kde.org/kanagram/"
pkgdesc="Letter Order Game"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdeclarative-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	knewstuff-dev
	libkeduvocdocument-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	samurai
	sonnet-dev
	"
_repo_url="https://invent.kde.org/education/kanagram.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kanagram-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
a5f732c0d5f92320fbdfd827c276ac726eddfcf32193afb33fb5cf643dacf3e98f1fd70c85b10cc9fab917719e6e25c9a5d8c2f71e0482d926c8983916b4cf7e  kanagram-23.04.3.tar.xz
"
