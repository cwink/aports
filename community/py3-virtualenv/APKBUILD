# Contributor: Sam Dodrill <shadowh511@gmail.com>
# Contributor: TBK <alpine@jjtc.eu>
# Maintainer: Sam Dodrill <shadowh511@gmail.com>
pkgname=py3-virtualenv
pkgver=20.24.2
pkgrel=0
pkgdesc="Virtual Python3 Environment builder"
options="!check" # Requires unpackaged 'flaky'
url="https://virtualenv.pypa.io/en/latest/"
arch="noarch"
license="MIT"
depends="py3-platformdirs py3-distlib py3-filelock"
makedepends="
	py3-gpep517
	py3-hatchling
	py3-hatch-vcs
	py3-wheel
	"
checkdepends="py3-pytest py3-six"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/v/virtualenv/virtualenv-$pkgver.tar.gz"
builddir="$srcdir/virtualenv-$pkgver"

replaces="py-virtualenv" # Backwards compatibility
provides="py-virtualenv=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 setup.py test
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
776924af470969503adec4ace6da626828ed0a41d17dcc88ef5b0166bed12f98862c6482a63f79d370cd9226c2b8e118933045d89369486ddad3c4e05c0db713  virtualenv-20.24.2.tar.gz
"
