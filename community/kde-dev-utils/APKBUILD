# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kde-dev-utils
pkgver=23.04.3
pkgrel=1
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/development/"
pkgdesc="Small utilities for developers using KDE/Qt libs/frameworks"
license="(LGPL-2.0-only OR LGPL-3.0-only) AND GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kcoreaddons-dev
	ki18n-dev
	kparts-dev
	kwidgetsaddons-dev
	qt5-qttools-dev
	samurai
	"
_repo_url="https://invent.kde.org/sdk/kde-dev-utils.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kde-dev-utils-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
1779a2667f7509abe4159f247a00339fdb6f099fc4865a9a82204cf54d610d4dea56f609b0a902dae9be44541cce8bfd607e2c9ee4024e3640825a5c714cc1e0  kde-dev-utils-23.04.3.tar.xz
"
