# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=postgresql-orafce
_pkgname=orafce
pkgver=4.5.0
_pkgver=VERSION_${pkgver//./_}
pkgrel=0
pkgdesc="Oracle's compatibility functions and packages for PostgreSQL"
url="https://github.com/orafce/orafce"
arch="all"
license="0BSD"
makedepends="bison flex postgresql-dev"
subpackages="
	$pkgname-bitcode
	$pkgname-doc
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/orafce/$_pkgname/archive/$_pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$_pkgver"
options="!check"  # XXX: installcheck requires running PostgreSQL

build() {
	make USE_PGXS=1 all
}

package() {
	_pgver=$(pg_config --major-version)
	depends="postgresql$_pgver"

	make USE_PGXS=1 DESTDIR="$pkgdir" install

	cd "$pkgdir"
	mv ./usr/share/doc/postgresql$_pgver/extension \
		./usr/share/doc/$pkgname
	rmdir ./usr/share/doc/postgresql$_pgver
}

bitcode() {
	_pgver=$(pg_config --major-version)
	pkgdesc="$pkgdesc (bitcode for JIT)"
	depends="$pkgname=$pkgver-r$pkgrel"
	install_if="postgresql$_pgver-jit $pkgname=$pkgver-r$pkgrel"

	amove usr/lib/postgresql*/bitcode
}

sha512sums="
1d71078154feb114b58c1a05e553b9c91ddde09c38f05ff788b40cc3fed1b19dadca3438b3ff36d4b515c706c69091310f5301297c1d687514864b82bd074a83  postgresql-orafce-4.5.0.tar.gz
"
