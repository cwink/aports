# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=ki18n
pkgver=5.108.0
pkgrel=2
pkgdesc="Advanced internationalization framework"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.0-or-later AND (LGPL-2.1-only OR LGPL-3.0-or-later)"
depends_dev="
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	doxygen
	graphviz
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/frameworks/ki18n.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/ki18n-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/ki18n.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	# ki18n-klocalizedstringtest, kcountrytest, kcountrysubdivisiontest and kcatalogtest are broken
	xvfb-run ctest --test-dir build --output-on-failure -E "(ki18n-klocalizedstring|kcountry|kcountrysubdivision|kcatalog)test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
fc3abfb724197b79ccf5cba01cfab11e1c5838a3314fae4e9b2c6b1833f53dea345aebeb8f6aa182e471ff2922422bd9d59df6d0fc45086993133348125495d2  ki18n-5.108.0.tar.xz
"
