# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=caddy
pkgver=2.7.3
pkgrel=0
pkgdesc="Fast, multi-platform web server with automatic HTTPS"
url="https://caddyserver.com/"
license="Apache-2.0"
arch="all"
depends="ca-certificates"
makedepends="go"
subpackages="$pkgname-openrc"
pkgusers="$pkgname"
pkggroups="$pkgname"
install="$pkgname.pre-install"
source="https://github.com/caddyserver/caddy/archive/v$pkgver/caddy-$pkgver.tar.gz
	$pkgname.initd
	Caddyfile

	fix-go-1.21-compat.patch
	"
options="net" # for downloading Go modules

export CGO_ENABLED=0
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -o bin/caddy ./cmd/caddy
}

check() {
	go test ./...
}

package() {
	install -Dm755 bin/caddy "$pkgdir"/usr/sbin/caddy

	install -Dm755 "$srcdir"/$pkgname.initd "$pkgdir"/etc/init.d/$pkgname
	install -Dm644 "$srcdir"/Caddyfile "$pkgdir"/etc/$pkgname/Caddyfile
}

sha512sums="
c7cd716c0d0972b1556eb8575f16aa8b4fa4b784815569da0552888a4580278980f1bc2698eaa143303c479049c83d1d20e331c558c24f9763ec0053d38e7e46  caddy-2.7.3.tar.gz
5dec305ee9b51d59a25d2c9c02d6d4e60bfc83ce3329f750f3c7d59ff7b5a4e844b0d999fa989cdaa37dbf086fefe82aec9351b08620fe8da9818ececc1436f0  caddy.initd
d3110dd79f7d5e602a34d42569104dc97603994e42daf5f6b105303a3d034b52b91ef5fb156d5bf7b7a3a58ec0aeff58afc402618d0555af053771952a866f76  Caddyfile
ba97ddd47ab77594a91b52a2196786b585c68cda0b3cafea8ede2981787bfa47aafc5693877c4c8287858b3b2cdbab3e784be2a3ddf056a2dd88eab37e523a1b  fix-go-1.21-compat.patch
"
