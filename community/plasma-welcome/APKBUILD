# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=plasma-welcome
pkgver=5.27.7
pkgrel=1
pkgdesc="A friendly onboarding wizard for Plasma"
# armhf blocked by qt5-qtdeclarative
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine -> kaccounts-integration
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
depends="kirigami2"
makedepends="
	extra-cmake-modules
	kaccounts-integration-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kdeclarative-dev
	ki18n-dev
	kio-dev
	kirigami2-dev
	knewstuff-dev
	knotifications-dev
	kservice-dev
	kuserfeedback-dev
	kwindowsystem-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtquickcontrols2-dev
	qt5-qtsvg-dev
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
_repo_url="https://invent.kde.org/plasma/plasma-welcome.git"
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-welcome-$pkgver.tar.xz"
subpackages="$pkgname-lang"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/plasma/plasma-welcome.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
a6f25593c85f35f274db6772b803a4a6aa73d6035837bf50d7e7ccaf4152288a5d44753567ff97e96ea36f53b36cb26623ef1c1500219d109e72e4c3f6e461ce  plasma-welcome-5.27.7.tar.xz
"
