# Contributor: guddaff <guddaff@protonmail.com>
# Maintainer: guddaff <guddaff@protonmail.com>
pkgname=bottom
pkgver=0.9.4
pkgrel=0
pkgdesc="Graphical process/system monitor with a customizable interface"
url="https://github.com/ClementTsang/bottom"
# s390x: fails to build nix crate
arch="all !s390x"
license="MIT"
makedepends="cargo cargo-auditable"
subpackages="
	$pkgname-fish-completion
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-doc
	"
source="https://github.com/ClementTsang/bottom/archive/$pkgver/bottom-$pkgver.tar.gz"

prepare() {
	default_prepare
	cargo fetch --target="$CTARGET" --locked
}

build() {
	BTM_GENERATE=true cargo auditable build --frozen --release
}

check() {
	CARGO_HUSKY_DONT_INSTALL_HOOKS=true cargo test --frozen
}

package() {
	install -Dm755 target/release/btm -t "$pkgdir"/usr/bin/

	install -Dm644 sample_configs/default_config.toml -t "$pkgdir"/usr/share/doc/$pkgname/

	cd target/tmp/bottom/completion
	install -Dm644 _btm "$pkgdir"/usr/share/zsh/site-functions/_btm
	install -Dm644 btm.bash "$pkgdir"/usr/share/bash-completion/completions/btm
	install -Dm644 btm.fish "$pkgdir"/usr/share/fish/vendor_completions.d//btm.fish
}

sha512sums="
6b197b7fe55868d6531b155b5ef57fa69a5f17c1a7b1b98ae4e0c85d66fcf055987cc3485ffe1c751294b03ae39ac9985d48723cd402db32714836e624a54c36  bottom-0.9.4.tar.gz
"
