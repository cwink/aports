# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=breeze-plymouth
pkgver=5.27.7
pkgrel=1
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://kde.org/"
pkgdesc="Breeze theme for Plymouth"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	plymouth-dev
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
_repo_url="https://invent.kde.org/plasma/breeze-plymouth.git"
source="https://download.kde.org/$_rel/plasma/$pkgver/breeze-plymouth-$pkgver.tar.xz"
options="!check" # No tests

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/plasma/breeze-plymouth.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DDISTRO_NAME="$(grep PRETTY_NAME /etc/os-release | awk -F  = '{ print $2 }')" \
		-DDISTRO_VERSION="$(grep VERSION_ID /etc/os-release | awk -F  = '{ print $2 }')"
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
e9034561c520ca055b273aa1112089bebfc2f644ee92026d8fd034724ec8d3c7ffd729e7a6fcc1f35495c7ccc2e00f4d57660daa0d3342b35fff6c6999dbe371  breeze-plymouth-5.27.7.tar.xz
"
