# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=akonadi-contacts
pkgver=23.04.3
pkgrel=1
pkgdesc="Libraries and daemons to implement Contact Management in Akonadi"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by akonadi
# ppc64le blocked by qt5-qtwebengine -> akonadi
arch="all !armhf !s390x !riscv64 !ppc64le"
url="https://community.kde.org/KDE_PIM"
license="LGPL-2.0-or-later AND GPL-2.0-or-later AND BSD-3-Clause"
depends_dev="
	akonadi-dev>=$pkgver
	gpgme-dev
	grantlee-dev
	grantleetheme-dev
	kcodecs-dev
	kcompletion-dev
	kconfig-dev
	kcontacts-dev
	kdbusaddons-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kmime-dev
	kservice-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libkleo-dev
	prison-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/pim/akonadi-contacts.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/akonadi-contacts-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
a0447eabcacb76b3cc8982760e5d9ef027537e48279353c47a6f361d4e4ab4421ddb2bc0f2aeda9121cd56721a4d98f551311d0d3254e6d069054abdb3bd2ebc  akonadi-contacts-23.04.3.tar.xz
"
