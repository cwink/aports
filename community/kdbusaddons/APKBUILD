# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kdbusaddons
pkgver=5.108.0
pkgrel=2
pkgdesc="Addons to QtDBus"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only OR LGPL-3.0-only"
makedepends="
	doxygen
	extra-cmake-modules
	graphviz
	qt5-qttools-dev
	shared-mime-info
	samurai
	"
checkdepends="dbus"
_repo_url="https://invent.kde.org/frameworks/kdbusaddons.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kdbusaddons-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/kdbusaddons.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	dbus-run-session -- ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
adcfebb3afe735f8cd1c98e20c0711966e6e425ecd1d1631431e06b0fdda703c519becea2086f19109d44ca48dbf9cadd495155ea41f69f2813a427933ba3c26  kdbusaddons-5.108.0.tar.xz
"
