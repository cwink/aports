# Contributor: Carlo Landmeter <clandmeter@gmail.com>
# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Leonardo Arena <larena@alpinelinux.org>
pkgname=lxd
pkgver=5.0.2
pkgrel=14
pkgdesc="A container hypervisor and a new user experience for LXC - 'LTS' release channel"
url="https://ubuntu.com/lxd"
arch="all"
license="Apache-2.0"
depends="acl
	attr
	ca-certificates
	cgmanager
	dbus
	dnsmasq
	lxc
	iproute2
	iptables
	netcat-openbsd
	rsync
	squashfs-tools
	shadow-uidmap
	tar
	xz
	"
makedepends="acl-dev
	autoconf
	automake
	dqlite-dev
	eudev-dev
	gettext-dev
	go
	intltool
	libcap-dev
	libtool
	libuv-dev
	linux-headers
	lxc-dev
	lz4-dev
	raft-dev
	sqlite-dev
	tcl-dev
	"
subpackages="$pkgname-client
	$pkgname-scripts:scripts
	$pkgname-bash-completion:bashcomp:noarch
	$pkgname-openrc
	$pkgname-vm
	"
provides=$pkgname-lts=$pkgver-r$pkgrel
install="$pkgname.pre-install"
options="!check" # FIXME: several tests failing
source="$pkgname-$pkgver-2.tar.gz::https://github.com/canonical/lxd/archive/refs/tags/lxd-$pkgver.tar.gz
	$pkgname.confd
	$pkgname.initd
	"
builddir="$srcdir/lxd-lxd-$pkgver"

_tools="lxc fuidshift lxc-to-lxd lxd-benchmark lxd-agent"
_project="github.com/lxc/lxd"

export GOFLAGS="$GOFLAGS -tags=libsqlite3"
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

prepare() {
	default_prepare
	go mod download
}

build() {
	export CGO_CFLAGS="-I/usr/include/raft -I/usr/include/dqlite"
	export CGO_LDFLAGS="$LDFLAGS -lintl"
	export CGO_LDFLAGS_ALLOW="(-Wl,-wrap,pthread_create)|(-Wl,-z,now)"

	mkdir bin
	for tool in lxd $_tools; do
		go build -v -o bin/$tool ./$tool
	done
}

package() {
	install -Dm755 bin/lxd "$pkgdir"/usr/sbin/lxd

	for tool in $_tools; do
		install -Dm755 bin/$tool "$pkgdir"/usr/bin/$tool
	done

	install -Dm755 "$srcdir"/$pkgname.initd \
		"$pkgdir"/etc/init.d/lxd
	install -Dm644 "$srcdir"/$pkgname.confd \
		"$pkgdir"/etc/conf.d/lxd

	install -Dm644 scripts/bash/lxd-client \
		"$pkgdir"/usr/share/bash-completion/completions/lxd-client

	install -Dm755 scripts/empty-lxd.sh \
		"$pkgdir"/usr/bin/empty-lxd.sh
}

client() {
	pkgdesc="LXD CLI client"
	amove usr/bin/lxc
}

scripts() {
	pkgdesc="LXD scripts"
	depends="$pkgname py3-lxc"
	provides=$pkgname-lts-scripts=$pkgver-r$pkgrel

	amove usr/bin
}

vm() {
	pkgdesc="Install packages required to run VMs under LXD"
	depends="qemu-system-x86_64
		qemu-chardev-spice
		qemu-hw-usb-redirect
		qemu-hw-display-virtio-vga
		qemu-img
		qemu-ui-spice-core
		lxd-scripts
		ovmf
		sgdisk
		util-linux-misc
		virtiofsd
		"
	install -d "$subpkgdir"
}

openrc() {
	provides=$pkgname-lts-openrc=$pkgver-r$pkgrel
	default_openrc
}

check() {
	LXD_OFFLINE=true make check
}

sha512sums="
2425be1c8a3f7c1ed01d00a13750caa5890b232aa7b45a57044c6954020bf02d793f4b46d101fca41c32a2bace9c164fc40a37c0df1e8e1efc1ecd57e220711f  lxd-5.0.2-2.tar.gz
5af20408f4bbbcd20b60c610c26d9aa52798d4f970ef649ea1d3b14bdbb0ee87ab52166551a4fcf724a6641d4afdcdb07ef4d6da4a0b49448d2673e27b1b2f40  lxd.confd
ed1fee1b779364da20451377c3df1c32ca5de5ef5fc65e9b4c802a58c2b7f1a13814e17610759f458ac9698a0c79e01a32d807bf5e125042a70447899c27e72b  lxd.initd
"
